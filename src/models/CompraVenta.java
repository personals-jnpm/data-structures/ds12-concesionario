package models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CompraVenta {

    private List<Particular> carrosParticularesDisponibles;
    private List<Publico> carrosPublicosDisponibles;
    private List<Vehiculo> carrosVendidos;

    private int posParticulares;
    private int posPublicos;

    public CompraVenta() {
        carrosParticularesDisponibles = new ArrayList<>();
        carrosPublicosDisponibles = new ArrayList<>();
        carrosVendidos = new ArrayList<>();
        posParticulares = 7;
        posPublicos = 7;
        initVehicles();
    }

    public List<Particular> getCarrosParticularesDisponibles() {
        return carrosParticularesDisponibles;
    }

    public void setCarrosParticularesDisponibles(List<Particular> carrosParticularesDisponibles) {
        this.carrosParticularesDisponibles = carrosParticularesDisponibles;
    }

    public List<Publico> getCarrosPublicosDisponibles() {
        return carrosPublicosDisponibles;
    }

    public void setCarrosPublicosDisponibles(List<Publico> carrosPublicosDisponibles) {
        this.carrosPublicosDisponibles = carrosPublicosDisponibles;
    }

    public List<Vehiculo> getCarrosVendidos() {
        return carrosVendidos;
    }

    public void setCarrosVendidos(List<Vehiculo> carrosVendidos) {
        this.carrosVendidos = carrosVendidos;
    }

    public void registrarVehiculo(Vehiculo vehiculo) {
        if (vehiculo instanceof Publico) {
            registrarVehiculoPublico((Publico) vehiculo);
        } else {
            registrarVehiculoParticular((Particular) vehiculo);
        }
    }

    // Métodos agregados
    private void registrarVehiculoParticular(Particular particular) {
        particular.setId(posParticulares++);
        carrosParticularesDisponibles.add(particular);
    }

    private void registrarVehiculoPublico(Publico publico) {
        publico.setId(posPublicos++);
        carrosPublicosDisponibles.add(publico);
    }

    public void venderVehiculo(Vehiculo vehiculo) {
        carrosVendidos.add(vehiculo);

        if (vehiculo instanceof Publico) {
            carrosPublicosDisponibles.remove(vehiculo);
        } else {
            carrosParticularesDisponibles.remove(vehiculo);
        }
    }

    public void mostrarVehiculos() {
        imprimirVehiculos("Vehiculos vendidos", carrosVendidos);
    }

    public void mostrarVehiculosParticulares() {
        System.out.println("\nParticulares: [");
        carrosParticularesDisponibles.forEach(particular-> System.out.println("\t" + particular.toString()));
        System.out.println("]");
    }

    public void mostrarVehiculosPublicos() {
        System.out.println("\nPublicos: [");
        carrosPublicosDisponibles.forEach(publico-> System.out.println("\t" + publico.toString()));
        System.out.println("]");
    }

    public void imprimirVehiculos(String name, List<Vehiculo> vehiculos) {
        System.out.println("\n" + name + ": [");
        vehiculos.forEach(vehiculo -> System.out.println("\t" + vehiculo.toString()));
        System.out.println("]");
    }

    public void buscarVehiculoTipoPrecio(boolean particular, int min, int max) {
        List<Vehiculo> vehiculos = new ArrayList<>();
        if (particular) {
            for (Vehiculo vehiculo : carrosParticularesDisponibles) {
                if (vehiculo.getValor() >= min && vehiculo.getValor() <= max) {
                    vehiculos.add(vehiculo);
                }
            }
        } else {
            for (Vehiculo vehiculo : carrosPublicosDisponibles) {
                if (vehiculo.getValor() >= min && vehiculo.getValor() <= max) {
                    vehiculos.add(vehiculo);
                }
            }
        }
        imprimirVehiculos("Vehiculos " + (particular ? "Particulares" : "Públicos") + " entre $" +
                min + " y $" + max, vehiculos);
    }

    public void buscarVehiculoTipoModelo(boolean particular, int modelo) {
        List<Vehiculo> vehiculos = new ArrayList<>();
        if (particular) {
            for (Vehiculo vehiculo : carrosParticularesDisponibles) {
                if (vehiculo.getModelo() == modelo) {
                    vehiculos.add(vehiculo);
                }
            }
        } else {
            for (Vehiculo vehiculo : carrosPublicosDisponibles) {
                if (vehiculo.getModelo() == modelo) {
                    vehiculos.add(vehiculo);
                }
            }
        }
        imprimirVehiculos("Vehiculos " + ((particular) ? "Particulares" : "Públicos") + " por modelo " +
                modelo, vehiculos);
    }

    public void ordenarPorTipoModelo(boolean particular) {
        if (particular) {
            carrosParticularesDisponibles.sort(Comparator.comparingInt(Vehiculo::getModelo));
            mostrarVehiculosParticulares();
        } else {
            carrosPublicosDisponibles.sort(Comparator.comparingInt(Vehiculo::getModelo));
            mostrarVehiculosPublicos();
        }
    }

    public void ordenarPorTipoPrecio(boolean particular) {
        List<Vehiculo> vehiculosOrdenados;
        if (particular) {
            vehiculosOrdenados = carrosParticularesDisponibles.stream()
                    .sorted(Comparator.comparing(Particular::getValor))
                    .collect(Collectors.toList());
            imprimirVehiculos("Vehiculos particulares ordenados por precio", vehiculosOrdenados);
        } else {
            vehiculosOrdenados = carrosPublicosDisponibles.stream()
                    .sorted(Comparator.comparing(Publico::getValor))
                    .collect(Collectors.toList());
            imprimirVehiculos("Vehiculos públicos ordenados por precio ", vehiculosOrdenados);
        }
    }

    private void initVehicles() {
        carrosParticularesDisponibles.add(new Particular(0, 250000, "Diésel", 2018, "Nuevo", 4, "Verde", "Motor1"));
        carrosParticularesDisponibles.add(new Particular(1, 300000, "Gasolina", 98, "Nuevo", 2, "Azul", "Motor2"));
        carrosParticularesDisponibles.add(new Particular(2, 126000, "Gasolina", 2015, "Usado", 2, "Amarillo", "Motor3"));
        carrosParticularesDisponibles.add(new Particular(3, 458000, "Híbrido", 95, "Usado", 4, "Purpura", "Motor4"));
        carrosParticularesDisponibles.add(new Particular(4, 725000, "Diésel", 2007, "Usado", 2, "Rojo", "Motor5"));
        carrosParticularesDisponibles.add(new Particular(5, 79000, "Gasolina", 2011, "Nuevo", 4, "Café", "Motor6"));
        carrosParticularesDisponibles.add(new Particular(6, 56000, "Eléctrico", 85, "Usado", 6, "Verde", "Motor7"));

        carrosPublicosDisponibles.add(new Publico(0, 100000, "ACPM", 93, "Usado", 100, "Bus", true));
        carrosPublicosDisponibles.add(new Publico(1, 350000, "Gasolina", 2014, "Nuevo", 10, "Taxi", true));
        carrosPublicosDisponibles.add(new Publico(2, 420000, "Eléctrico", 2020, "Nuevo", 10, "Buseta escolar", false));
        carrosPublicosDisponibles.add(new Publico(3, 125000, "Diésel", 80, "Nuevo", 10, "Bus", true));
        carrosPublicosDisponibles.add(new Publico(4, 800000, "ACPM", 2000, "Usado", 10, "Tracto mula", false));
        carrosPublicosDisponibles.add(new Publico(5, 526000, "Gasolina", 2018, "Nuevo", 10, "Transmilenio", false));
        carrosPublicosDisponibles.add(new Publico(6, 311000, "Híbrido", 2005, "Usado", 10, "Taxi", true));

        venderVehiculo(new Particular(4, 725000, "Diésel", 2007, "Usado", 2, "Rojo", "Motor5"));
        venderVehiculo(new Publico(6, 311000, "Híbrido", 2005, "Usado", 10, "Taxi", true));
    }

}
