package models;

public class Particular extends Vehiculo {

    private int numPuertas;
    private String color;
    private String tipoMotor;

    public Particular() {
        super();
    }

    public Particular(int id, double valor, String tipoCombustible, int modelo, String estado, int numPuertas, String color, String tipoMotor) {
        super(id, valor, tipoCombustible, modelo, estado);
        this.numPuertas = numPuertas;
        this.color = color;
        this.tipoMotor = tipoMotor;
    }

    public Particular(double valor, String tipoCombustible, int modelo, String estado, int numPuertas, String color, String tipoMotor) {
        super(valor, tipoCombustible, modelo, estado);
        this.numPuertas = numPuertas;
        this.color = color;
        this.tipoMotor = tipoMotor;
    }

    public int getNumPuertas() {
        return numPuertas;
    }

    public void setNumPuertas(int numPuertas) {
        this.numPuertas = numPuertas;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(String tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    @Override
    public String toString() {
        return "Particular: {" +
                super.toString() +
                ", numPuertas: " + numPuertas +
                ", color: '" + color + '\'' +
                ", tipoMotor: '" + tipoMotor + '\'' +
                '}';
    }
}
