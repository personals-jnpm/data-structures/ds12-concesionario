package models;

public class Vehiculo {

    private int id;
    private double valor;
    private String tipoCombustible;
    private int modelo;
    private String estado;

    public Vehiculo() {
    }

    public Vehiculo(int id, double valor, String tipoCombustible, int modelo, String estado) {
        this.id = id;
        this.valor = valor;
        this.tipoCombustible = tipoCombustible;
        this.modelo = modelo;
        this.estado = estado;
    }

    public Vehiculo(double valor, String tipoCombustible, int modelo, String estado) {
        this.valor = valor;
        this.tipoCombustible = tipoCombustible;
        this.modelo = modelo;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getTipoCombustible() {
        return tipoCombustible;
    }

    public void setTipoCombustible(String tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }

    public int getModelo() {
        return modelo;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return  "id=" + id +
                ", valor = " + valor +
                ", tipoCombustible = " + tipoCombustible +
                ", modelo = " + modelo +
                ", estado = " + estado;
    }
}
