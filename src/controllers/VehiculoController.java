package controllers;

import models.CompraVenta;
import models.Particular;
import models.Publico;

public class VehiculoController {

    private final CompraVenta compraVenta;

    public VehiculoController() {
        compraVenta = new CompraVenta();
    }

    public void registrarVehiculoParticular(double valor, String tipoCombustible, int modelo, String estado,
                                            int numPuertas, String color, String tipoMotor) {
        compraVenta.registrarVehiculo(new Particular(valor, tipoCombustible, modelo, estado, numPuertas, color, tipoMotor));
    }

    public void registrarVehiculoPublico(double valor, String tipoCombustible, int modelo, String estado, int capacidad,
                                         String tipo, boolean cupo) {
        compraVenta.registrarVehiculo(new Publico(valor, tipoCombustible, modelo, estado, capacidad, tipo, cupo));
    }

    public void venderVehiculoParticular(int id) {
        try {
            compraVenta.venderVehiculo(buscarVehiculoParticular(id));
        }catch (NullPointerException e){
            System.err.println("  Error: Código del vehículo particular no válido");
        }
    }

    public void venderVehiculoPublico(int id){
        try {
            compraVenta.venderVehiculo(buscarVehiculoPublico(id));
        }catch (NullPointerException e){
            System.err.println("  Error: Código del vehículo público no válido");
        }
    }

    public void mostrarVehiculosVendidos() {
        compraVenta.mostrarVehiculos();
    }

    public void mostrarVehiculosParticulares() {
        compraVenta.mostrarVehiculosParticulares();
    }

    public void mostrarVehiculosPublicos() {
        compraVenta.mostrarVehiculosPublicos();
    }

    public void buscarVehiculoPorPrecio(boolean particular, int min, int max) {
        compraVenta.buscarVehiculoTipoPrecio(particular, min, max);
    }

    public void buscarVehiculoPorModelo(boolean particular, int modelo) {
        compraVenta.buscarVehiculoTipoModelo(particular, modelo);
    }

    public void ordenarPorModelo(boolean option) {
        compraVenta.ordenarPorTipoModelo(option);
    }

    public void ordenarPorPrecio(boolean option) {
        compraVenta.ordenarPorTipoPrecio(option);
    }

    private Particular buscarVehiculoParticular(int id) throws NullPointerException{
        for (Particular particular:compraVenta.getCarrosParticularesDisponibles()) {
            if (particular.getId() == id) {
                return particular;
            }
        }
        return null;
    }

    private Publico buscarVehiculoPublico(int id) throws NullPointerException{
        for (Publico publico:compraVenta.getCarrosPublicosDisponibles()) {
            if (publico.getId() == id) {
                return publico;
            }
        }
        return null;
    }
}
