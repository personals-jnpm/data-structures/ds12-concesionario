import controllers.VehiculoController;
import utiliy.Keyboard;

public class Inicial {

    public static Keyboard keyboard = new Keyboard();
    public static VehiculoController vehiculoController = new VehiculoController();
    public static final String TIPO_VEHICULO = "  Ingrese el tipo de carro, (Particular = 1, Público = 2): ";

    public static void main(String[] args) {
        Inicial inicial = new Inicial();
        inicial.menu();
        int option;
        do {
            System.out.print("\nSeleccione una opción en el menú principal: ");
            option = keyboard.readIntegerDefault(-1);
            switch (option) {
                case 0 -> System.out.println(" El programa ha finalizado");
                case 1 -> inicial.registrarVehiculoParticular();
                case 2 -> inicial.registrarVehiculoPublico();
                case 3 -> inicial.venderVehiculoParticular();
                case 4 -> inicial.venderVehiculoPublico();
                case 5 -> inicial.mostrarVehiculosVendidos();
                case 6 -> inicial.mostrarVehiculosParticulares();
                case 7 -> inicial.mostrarVehiculosPublicos();
                case 8 -> inicial.buscarVehiculoPorPrecio();
                case 9 -> inicial.buscarVehiculoPorModelo();
                case 10 -> inicial.ordenarPorModelo();
                case 11 -> inicial.ordenarPorPrecio();
                default -> System.out.println(" ¡Opción no disponible en el menú principal!");
            }
        } while (option != 0);
    }

    public void menu() {
        System.out.println("╔════════════════════════════════════════════════════════╗");
        System.out.println("╠------------------Compra venta de autos-----------------╣");
        System.out.println("║════════════════════════════════════════════════════════║");
        System.out.println("║   1. Registrar vehículo particular                     ║");
        System.out.println("║   2. Registrar vehículo público                        ║");
        System.out.println("║   3. Vender vehículo particular                        ║");
        System.out.println("║   4. Vender vehículo público                           ║");
        System.out.println("║════════════════════════════════════════════════════════║");
        System.out.println("║   5. Mostrar vehículos vendidos                        ║");
        System.out.println("║   6. Mostrar vehículos particulares disponibles        ║");
        System.out.println("║   7. Mostrar vehículos públicos disponibles            ║");
        System.out.println("║════════════════════════════════════════════════════════║");
        System.out.println("║   8. Buscar vehículos por precio                       ║");
        System.out.println("║   9. Buscar vehículos por modelo                       ║");
        System.out.println("║   10. Ordenar vehículos por modelo                     ║");
        System.out.println("║   11. Ordenar vehículos por precio                     ║");
        System.out.println("║════════════════════════════════════════════════════════║");
        System.out.println("║   0. Salir                                             ║");
        System.out.println("╚════════════════════════════════════════════════════════╝");
    }

    public void registrarVehiculoParticular() {
        double valor = keyboard.readValidPositiveDouble("  Ingrese el precio del vehículo: ");
        String tipoCombustible = keyboard.readLineDefault("  Ingrese el tipo de combustible: ");
        int modelo = keyboard.readValidPositiveInteger("  Ingrese el modelo del vehículo: ");
        String estado = keyboard.readLineDefault("  Ingrese el estado del vehículo: ");
        int numPuertas = keyboard.readValidPositiveInteger("  Ingrese le número de puertas: ");
        String color = keyboard.readLineDefault("  Ingrese el color del vehículo: ");
        String tipoMotor = keyboard.readLineDefault("  Ingrese el tipo de motor del vehículo: ");

        vehiculoController.registrarVehiculoParticular(valor, tipoCombustible, modelo, estado, numPuertas, color,
                tipoMotor);
    }

    public void registrarVehiculoPublico() {
        double valor = keyboard.readValidPositiveDouble("  Ingrese el precio del vehículo: ");
        String tipoCombustible = keyboard.readLineDefault("  Ingrese el tipo de combustible: ");
        int modelo = keyboard.readValidPositiveInteger("  Ingrese el modelo del vehículo: ");
        String estado = keyboard.readLineDefault("  Ingrese el estado del vehículo: ");
        int capacidad = keyboard.readValidPositiveInteger("  Ingrese la capacidad del vehículo: ");
        String tipo = keyboard.readLineDefault("  Ingrese el tipo de vehículo público: ");
        boolean cupo = keyboard.readValidBoolean("  Ingrese si el cupo está disponible (1 = cupo disponible, 2 = cupo ocupado): ") == 1;

        vehiculoController.registrarVehiculoPublico(valor, tipoCombustible, modelo, estado, capacidad, tipo, cupo);
    }

    public void venderVehiculoParticular() {
        mostrarVehiculosParticulares();
        vehiculoController.venderVehiculoParticular(keyboard.readValidPositiveInteger(
                "  Ingrese el código del vehículo particular: "));
    }

    public void venderVehiculoPublico() {
        mostrarVehiculosPublicos();
        vehiculoController.venderVehiculoPublico(keyboard.readValidPositiveInteger(
                "  Ingrese el código del vehículo público: "));
    }

    public void mostrarVehiculosVendidos() {
        vehiculoController.mostrarVehiculosVendidos();
    }

    public void mostrarVehiculosParticulares() {
        vehiculoController.mostrarVehiculosParticulares();
    }

    public void mostrarVehiculosPublicos() {
        vehiculoController.mostrarVehiculosPublicos();
    }

    public void buscarVehiculoPorPrecio() {
        boolean particular = keyboard.readValidBoolean(TIPO_VEHICULO) == 1;
        int min = keyboard.readValidPositiveInteger("  Ingrese el rango mínimo del carro: ");
        int max = keyboard.readValidPositiveInteger("  Ingrese el rango máximo del carro: ");
        vehiculoController.buscarVehiculoPorPrecio(particular, min, max);
    }

    public void buscarVehiculoPorModelo() {
        boolean particular = keyboard.readValidBoolean(TIPO_VEHICULO) == 1;
        int modelo = keyboard.readValidPositiveInteger("  Ingrese el modelo del vehículo: ");
        vehiculoController.buscarVehiculoPorModelo(particular, modelo);
    }

    public void ordenarPorModelo() {
        boolean particular = keyboard.readValidBoolean(TIPO_VEHICULO) == 1;
        vehiculoController.ordenarPorModelo(particular);
    }

    public void ordenarPorPrecio() {
        boolean particular = keyboard.readValidBoolean(TIPO_VEHICULO) == 1;
        vehiculoController.ordenarPorPrecio(particular);
    }
}
